# Search API Menu Boost

### Introduction

This module provides a way to increase or decrease the scoring of an item if it is part on one of the available menus on the site.

### Features

* Select menus where boosting will apply.
* Apply different boosting values per menu.


### Post-Installation

On the processor configuration settings for your Search API index, pick the menu where boosting will be enabled, then set the boosting value.